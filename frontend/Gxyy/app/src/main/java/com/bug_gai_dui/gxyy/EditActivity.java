package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

public class EditActivity extends AppCompatActivity  {
    public static Integer sex_change=1;

    String username;
    Integer uid;
    Integer select_sex_ig;
    RadioGroup sexChose;
    Integer status;
    String email_judge;
    LinearLayout send_change_word;
    String select_email_ig,select_name_ig,select_stu_no_ig;
    EditText email_text,verification_edit;
    boolean isVerificationCodeSend = false;
    Integer isNeedVerify=0;
    Button button_verification_code=null;
    String verification_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit);

        AtyContainer.getInstance().addActivity(this);
        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类

        uid=dbhelper.getUid();
        sexChose=(RadioGroup)findViewById(R.id.sex_chose);

        sexChose.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener( ) {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(select_email_ig, "onCheckedChanged: ");
                if (checkedId == R.id.man) {
                    select_sex_ig=1;
                } else if (checkedId == R.id.woman) {
                    select_sex_ig=2;
                }else{
                    select_sex_ig=0;
                }
            }
        });
        send_change_word=findViewById(R.id.linearLayout8);

        email_text=findViewById(R.id.edit_edit_mailbox);
        button_verification_code=(Button) this.findViewById(R.id.button_send_verification_modify);
        getUserInfo();

        send_change_word.setVisibility(View.INVISIBLE);
        email_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
//                Log.d(email_judge, "被执行了？: ");
//                Log.d(email_text.getText().toString(), "被执行了？: ");
                String emailMatcher="[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+";
                boolean isMatch = Pattern.matches(emailMatcher,email_text.getText().toString());
                if(isMatch==false)
                {
                    Toast.makeText(EditActivity.this, "邮箱不合法", Toast.LENGTH_SHORT).show();
                }
                if(email_judge.equals(email_text.getText().toString())){
                    if(send_change_word.getVisibility()==View.VISIBLE)
                    send_change_word.setVisibility(View.INVISIBLE);
                    isNeedVerify=0;
                }else{
                    send_change_word.setVisibility(View.VISIBLE);
                    isNeedVerify=1;
                }
            }
        });

        button_verification_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVerificationCode();
            }
        });


        Button post_info = null;
        post_info=(Button) this.findViewById(R.id.edit_button_confirm);


        post_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edit_username;
                edit_username=(EditText) EditActivity.this.findViewById(R.id.edit_edit_username);
                username=edit_username.getText().toString();
                verification_edit=(EditText) EditActivity.this.findViewById(R.id.editText_verification_code_modify);
                verification_code=verification_edit.getText().toString();
                TextView user,email,uid,stuno,userName;
                RadioGroup sex;
                user = findViewById(R.id.edit_edit_username);
                sex = findViewById(R.id.sex_chose);
                email = findViewById(R.id.edit_edit_mailbox);

                stuno = findViewById(R.id.edit_edit_studentNo);
                userName = findViewById(R.id.edit_USERName);
                username=user.getText().toString();
                select_email_ig=email.getText().toString();
                select_name_ig=userName.getText().toString();
                select_stu_no_ig=stuno.getText().toString();



                if(username.length() == 0||select_email_ig.length()==0||select_name_ig.length()==0||select_stu_no_ig.length()==0) {
                    Toast.makeText(EditActivity.this, username + " " + select_email_ig + " " + select_name_ig + " " + select_stu_no_ig, Toast.LENGTH_SHORT).show();
                    Toast.makeText(EditActivity.this, "似乎忘记填写了一些必要的东西", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (isNeedVerify==1&&verification_code.length()==0) {
                    Toast.makeText(EditActivity.this, "请填写验证码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(select_name_ig.length()>20)
                {
                    Toast.makeText(EditActivity.this, "真实姓名太长了", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(select_stu_no_ig.length()>10)
                {
                    Toast.makeText(EditActivity.this, "学号太长了", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(username.length() > 20||verification_code.length()>50)
                {
                    Toast.makeText(EditActivity.this, "有些个人信息长度是不是不太符合常识", Toast.LENGTH_SHORT).show();
                    return;
                }
                String emailMatcher="[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]+";

                //write your code here......
                boolean isMatch = Pattern.matches(emailMatcher,select_email_ig);
                if(isMatch==false)
                {

                    Toast.makeText(EditActivity.this, "邮箱不合法", Toast.LENGTH_SHORT).show();
                    return;

                }



                modifyUserInfo();
            }
        });




    }

    public void getUserInfo() {
        new Thread(()->{

            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("uid", uid);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever)+"/user_context") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(EditActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });   }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String string = null;
                            try {
                                string = response.body().string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            //Log.i("info", string + "");
                            try {
                                JSONObject jsonO = new JSONObject(string);
                                username=jsonO.getString("username");

                                select_sex_ig = jsonO.getInt("sex");
                                select_email_ig = jsonO.getString("email");
                                select_name_ig = jsonO.getString("name");
                                select_stu_no_ig = jsonO.getString("student No");
                                TextView user,email,uid,stuno,userName;
                                RadioGroup sex;
                                user = findViewById(R.id.edit_edit_username);
                                sex = findViewById(R.id.sex_chose);
                                email = findViewById(R.id.edit_edit_mailbox);
                                email_judge=select_email_ig;
                                stuno = findViewById(R.id.edit_edit_studentNo);
                                userName = findViewById(R.id.edit_USERName);

                                user.setText(username);
                                if(select_sex_ig==0)
                                {
                                    //do nothing
                                }else if(select_sex_ig==1){
                                    sex.check(R.id.man);
                                }else {
                                    sex.check(R.id.woman);
                                }
//                                sex.setText(select_sex_ig);
                                if(select_email_ig.length()!=0)
                                    email.setText(select_email_ig);

                                stuno.setText(select_stu_no_ig);
                                userName.setText(select_name_ig);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });


        }).start();
    }
    void sendVerificationCode() {
        if (email_text.getText().toString().trim().length() == 0) {
            Toast.makeText(EditActivity.this, "请输入邮箱", Toast.LENGTH_SHORT).show();
            return;
        }
        new Thread() {
            @Override
            public void run() {
                OkHttpClient okHttpClient = new OkHttpClient();

                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                JSONObject json = new JSONObject();
                try {
                    json.put("mailbox",email_text.getText().toString().trim());
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

                RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/send_verification_code")
                        .post(requestBody)
                        .build();

                Call call = okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String res = null;
                                try {
                                    res = Objects.requireNonNull(response.body()).string();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                                try {
                                    JSONObject jsonObject = new JSONObject(res);
                                    if (jsonObject.getInt("status") == 0) {
                                        Toast.makeText(EditActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                        isVerificationCodeSend = true;
                                        CountDownTimer countDownTimer = new CountDownTimer(60 * 1000,1000) {
                                            @Override
                                            public void onTick(long l) {
                                                button_verification_code.setText(l / 1000 + "s 后重试");
                                            }

                                            @Override
                                            public void onFinish() {
                                                button_verification_code.setClickable(true);
                                                button_verification_code.setText("发送验证码");
                                                cancel();
                                            }
                                        };
                                        countDownTimer.start();
                                        button_verification_code.setClickable(false);
                                    } else if (jsonObject.getInt("status") == 1) {
                                        Toast.makeText(EditActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                    } else if (jsonObject.getInt("status") == 2) {
                                        Toast.makeText(EditActivity.this, "此邮箱已被注册", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }
                });
            }
        }.start();
    }


    public void modifyUserInfo() {
        new Thread(()->{

            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            //开始对当前数据进行处理，符合期望要求


            try {

                json.put("uid", uid);
                json.put("sex", select_sex_ig);
                json.put("name", select_name_ig);
                json.put("student No", select_stu_no_ig);
                json.put("email", select_email_ig);
                json.put("verification_code", verification_code);
                json.put("need_verify", isNeedVerify);
                json.put("username", username);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever)+"/user_context_modify") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(EditActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });   }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String string = null;
                            try {
                                string = response.body().string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            //Log.i("info", string + "");
                            try {
                                JSONObject jsonO = new JSONObject(string);
                                if(jsonO.getString("status")!="null") {
                                    Log.d(jsonO.getString("status"), "run: ");
                                    status = jsonO.getInt("status");

                                    if (status == 0) {
                                        Toast.makeText(EditActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                                        LocalSQLite dbhelper= new LocalSQLite(EditActivity.this,"UserInfo.db",null,1);//第一步创建数据库帮助类
                                        dbhelper.updateUsername(username);
                                        sex_change=1;
                                        Log.d(sex_change.toString(), "sex_change: ");
                                        finish();
                                    } else if (status == 1) {
                                        Toast.makeText(EditActivity.this, "验证码错误或失效", Toast.LENGTH_SHORT).show();
                                    } else if(status == 3){
                                        Toast.makeText(EditActivity.this, "邮箱已被绑定", Toast.LENGTH_SHORT).show();

                                    }else {
                                        Toast.makeText(EditActivity.this, "用户名已经存在", Toast.LENGTH_SHORT).show();

                                    }
                                }


                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });


        }).start();
    }
}