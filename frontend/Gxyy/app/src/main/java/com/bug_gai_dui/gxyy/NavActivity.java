package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.amap.api.maps2d.model.LatLng;
import com.bug_gai_dui.gxyy.CircleImage.XCRoundImageView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bug_gai_dui.gxyy.databinding.ActivityNavBinding;

// 按钮
import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;

import android.os.Bundle;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.example.magicalpai.R;
import java.io.IOException;
import java.util.*;

import static android.app.PendingIntent.getActivity;


public class NavActivity extends AppCompatActivity {
    private String username;
    Integer uid;
    private ActivityNavBinding binding;
    static public List<Activities> activityList = new ArrayList<>();
    static public List<Activities> myactivityList = new ArrayList<>();
    static public int id;
    static public boolean isGM;
    static public String emali_get;
    static public int status;
    static public String activityname;
    static public int sex;
    static public int ongoing;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AtyContainer.getInstance().addActivity(this);
        binding = ActivityNavBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
        uid=dbhelper.getUid();
        username = dbhelper.getUserName();

//        getActivityInfo();
//        getMyActivitiesInfo();

        //getActivityInfo();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_nav);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NavActivity.this,BottomActivity.class);
        intent.putExtra("stop_app",true);
        startActivity(intent);
    }

    public void getActivityInfo(RecyclerView recyclerView) {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            Request request = new Request.Builder().url(getResources().getString(R.string.sever)+"/activity_menu") //创建活动接口
                    .get()
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(NavActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        JSONArray Arr=jObject.getJSONArray("menu");
                        activityList.clear();
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            Activities ac = new Activities(aStruct.getInt("id"),
                                    aStruct.getString("name"),
                                    aStruct.getInt("state"),
                                    new Date(aStruct.getJSONObject("date").getInt("y"),aStruct.getJSONObject("date").getInt("m"),aStruct.getJSONObject("date").getInt("d")));
                            activityList.add(ac);
                        }
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerView.setAdapter(new ActivitiesAdapter(activityList));
//                                    Toast.makeText(NavActivity.this, "获取活动成功", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                }
            });
        }).start();
    }

    public void getMyActivitiesInfo(RecyclerView recyclerView) {
        new Thread(()->{
            LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
            username = dbhelper.getUserName();
            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("username", username);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever)+"/signed_activity") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(NavActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });   }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        JSONArray Arr=jObject.getJSONArray("menu");
                        myactivityList.clear();
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            Activities ac = new Activities(aStruct.getInt("id"),
                                    aStruct.getString("name"),
                                    aStruct.getInt("state"),
                                    new Date(aStruct.getJSONObject("date").getInt("y"),aStruct.getJSONObject("date").getInt("m"),aStruct.getJSONObject("date").getInt("d")));
                            myactivityList.add(ac);
                        }
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerView.setAdapter(new ActivitiesAdapter(myactivityList));
//                                    Toast.makeText(NavActivity.this, "获取活动成功", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                }
            });
        }).start();
    }

    public void is_ongoing(ImageButton imageButton) {
        new Thread(()->{
            LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
            uid=dbhelper.getUid();
            username = dbhelper.getUserName();
            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("username", username);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever)+"/is_ongoing") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(NavActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });   }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        ongoing = jObject.getInt("state");  //0没有正在进行的活动 1有正在进行的活动
                        activityname = jObject.getString("activityname");
                        id = jObject.getInt("id");  //0没有正在进行的活动 1有正在进行的活动
                        status = jObject.getInt("status");  //0没有正在进行的活动 1有正在进行的活动
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(ongoing == 0){
                                        imageButton.setVisibility(View.GONE);
                                    }
                                    else{
                                        imageButton.setVisibility(View.VISIBLE);
                                        imageButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Intent intent = new Intent(NavActivity.this,OrienteeringActivity.class);
                                                intent.putExtra("ActivityId",id); // 正在进行活动的id
                                                intent.putExtra("ActivityTitle",activityname);// 正在进行活动的名称
                                                intent.putExtra("uStatus",status); // 权限
                                                startActivity(intent);
                                            }
                                        });
                                    }
//                                    Toast.makeText(NavActivity.this, "获取活动成功", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                }
            });
        }).start();
    }

    public void getUserInfo(XCRoundImageView touxiang, LinearLayout need_hide, LinearLayout needProcess,TextView edit_email) {
        new Thread(() -> {

            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
            uid=dbhelper.getUid();
            username = dbhelper.getUserName();
            try {
                json.put("uid", uid);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever) + "/user_context") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(NavActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String string = null;
                            try {
                                string = response.body().string();
                            } catch (IOException e) {
                                Toast.makeText(NavActivity.this, "网络波动，请休息一下再试", Toast.LENGTH_SHORT).show();
                            }
                            //Log.i("info", string + "");
                            try {
                                if(string!=null) {
                                    JSONObject jsonO = new JSONObject(string);


                                    isGM = jsonO.getBoolean("isGM");
                                    sex = jsonO.getInt("sex");
                                    emali_get=jsonO.getString("email");
                                    edit_email.setText(emali_get);
                                    if (sex == 1) {
                                        touxiang.setImageResource(R.drawable.touxiangman);
                                    } else {
                                        touxiang.setImageResource(R.drawable.touxiangwoman);
                                    }
                                    needProcess.setFocusable(false);
                                    if (isGM == true) {
                                        needProcess.setVisibility(View.VISIBLE);
                                        need_hide.setVisibility(View.VISIBLE);
                                    }
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });


        }).start();
    }
}