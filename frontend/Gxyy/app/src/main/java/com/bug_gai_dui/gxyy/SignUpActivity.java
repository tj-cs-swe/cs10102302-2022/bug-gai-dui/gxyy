package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {
    Integer userId;
    String userName;
    Integer activityId;
    String activityName;
    Integer teamMinPeople;
    Integer teamMaxPeople;
    ArrayList<Integer> memberIdList = new ArrayList<Integer>();
    ArrayList<String> memberNameList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        AtyContainer.getInstance().addActivity(this);
        Intent intent = getIntent();
        activityId = intent.getIntExtra("activity_id",0);

        LocalSQLite localSQLite = new LocalSQLite(SignUpActivity.this,"UserInfo.db",null,1);
        userId = localSQLite.getUid();
        userName = localSQLite.getUserName();
        TextView textview_captain = (TextView) findViewById(R.id.sign_up_textview_captain);
        textview_captain.setText(userName);

        setInfo();

        ImageButton imageButton_add = (ImageButton) findViewById(R.id.sign_up_button_add);
        imageButton_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText_search = (EditText) findViewById(R.id.sign_up_edittext_search);
                String member_name = editText_search.getText().toString().trim();
                if (member_name.length() != 0) {
                    if (memberNameList.size() + 1 == teamMaxPeople) {
                        Toast.makeText(SignUpActivity.this, "已达队伍人数上限", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!memberNameList.contains(member_name)) {
                            checkNameAdd(member_name);
                        } else {
                            Toast.makeText(SignUpActivity.this, "该成员已添加", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        ImageButton imageButton_delete = (ImageButton) findViewById(R.id.sign_up_button_reduce);
        imageButton_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(memberNameList.size() != 0) {
                    deleteMember();
                    memberNameList.remove(memberNameList.size() - 1);
                    memberIdList.remove(memberIdList.size() - 1);
                }
            }
        });

        Button button_confirm = (Button) findViewById(R.id.sign_up_button_confirm);
        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (memberNameList.size() + 1 < teamMinPeople) {
                    Toast.makeText(SignUpActivity.this, "队伍中人数少于下限", Toast.LENGTH_SHORT).show();
                } else {
                    confirm();
                }
            }
        });
    }

    void setInfo() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/activity_info") //获取活动信息接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SignUpActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                teamMinPeople = jsonObject.getInt("min_people_in_team");
                                teamMaxPeople = jsonObject.getInt("max_people_in_team");
                                activityName = jsonObject.getString("activity_name");

                                TextView textView_activity_name = (TextView) findViewById(R.id.sign_up_textview_activity_name);
                                textView_activity_name.setText(activityName);

                                TextView textView_min_people = (TextView) findViewById(R.id.sign_up_textview_min);
                                TextView textView_max_people = (TextView) findViewById(R.id.sign_up_textview_max);
                                textView_min_people.setText("最少：" + teamMinPeople);
                                textView_max_people.setText("最多：" + teamMaxPeople);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void checkNameAdd(String name) {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("activity_id", activityId);
                json.put("user_name", name);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/check_registered") //获取活动信息接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SignUpActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getInt("status") == 0) {
                                    Toast.makeText(SignUpActivity.this, "用户名不存在", Toast.LENGTH_SHORT).show();
                                } else if (jsonObject.getInt("status") == 1) {
                                    Toast.makeText(SignUpActivity.this, "该用户已报名此活动", Toast.LENGTH_SHORT).show();
                                } else if (jsonObject.getInt("status") == 2) {
                                    if (jsonObject.getInt("id") == userId) {
                                        Toast.makeText(SignUpActivity.this, "该用户为队长", Toast.LENGTH_SHORT).show();
                                    } else {
                                        addMember(name);
                                        memberNameList.add(name);
                                        memberIdList.add(jsonObject.getInt("id"));
                                    }
                                } else if (jsonObject.getInt("status") == 3) {
                                    Toast.makeText(SignUpActivity.this, "该用户此时间段已报名其他活动", Toast.LENGTH_SHORT).show();
                                } else if (jsonObject.getInt("status") == 2) {
                                    Toast.makeText(SignUpActivity.this, "该用户为管理员", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(SignUpActivity.this, "网络波动，请重试", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void addMember(String name) {
        LinearLayout linearLayout_member = (LinearLayout) findViewById(R.id.sign_up_layout_member);

        TextView textView = new TextView(this);
        textView.setText(name);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
        textView.setTextColor(0xFF000000);

        linearLayout_member.addView(textView);
    }

    void deleteMember() {
        LinearLayout linearLayout_member = (LinearLayout) findViewById(R.id.sign_up_layout_member);
        linearLayout_member.removeViewAt(linearLayout_member.getChildCount() - 1);
    }

    void confirm() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("activity_id", activityId);
                json.put("captain_id", userId);

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < memberIdList.size(); i++) {
                    jsonArray.put(memberIdList.get(i));
                }
                json.put("member_id", jsonArray);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/register_activity")
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SignUpActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getInt("status") == 0) {
                                    Toast.makeText(SignUpActivity.this, "报名成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else if (jsonObject.getInt("status") == 1) {
                                    Toast.makeText(SignUpActivity.this, "报名失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }
}