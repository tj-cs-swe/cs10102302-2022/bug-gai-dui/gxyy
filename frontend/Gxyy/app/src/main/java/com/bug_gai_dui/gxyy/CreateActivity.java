package com.bug_gai_dui.gxyy;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

public class CreateActivity extends AppCompatActivity {

    private EditText editTextActivityTime;
    private EditText editTextActivityEndTime;
    private EditText editTextActivityDeadline;
    private EditText editTextTeamMinPeople;
    private EditText editTextTeamMaxPeople;
    private Button btnRoadSet;
    private Button btnConfirm;
    Calendar calendar = Calendar.getInstance(Locale.CHINA);

    String activityName;
    Integer activityTimeYear;
    Integer activityTimeMonth;
    Integer activityTimeDay;
    Integer activityTimeHour;
    Integer activityTimeMinute;
    Integer activityEndTimeYear;
    Integer activityEndTimeMonth;
    Integer activityEndTimeDay;
    Integer activityEndTimeHour;
    Integer activityEndTimeMinute;
    Integer activityDeadlineYear;
    Integer activityDeadlineMonth;
    Integer activityDeadlineDay;
    Integer activityDeadlineHour;
    Integer activityDeadlineMinute;

    Integer teamMinPeople;
    Integer teamMaxPeople;

    ArrayList<Double> latitudeList = new ArrayList<>();
    ArrayList<Double> longitudeList = new ArrayList<>();
    ArrayList<Integer> typeList = new ArrayList<>();
    String activityIntro;
    String activityAttention;

    boolean isTimeSet = false;
    boolean isEndTimeSet = false;
    boolean isDeadlineSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        AtyContainer.getInstance().addActivity(this);
        editTextActivityTime = (EditText) findViewById(R.id.create_editText_time);
        editTextActivityEndTime = (EditText) findViewById(R.id.create_editText_end_time);
        editTextActivityDeadline = (EditText) findViewById(R.id.create_editText_deadline);
        editTextTeamMinPeople = (EditText) findViewById(R.id.create_editText_min);
        editTextTeamMaxPeople = (EditText) findViewById(R.id.create_editText_max);
        btnRoadSet = (Button) findViewById(R.id.roadSet);
        btnConfirm = (Button) findViewById(R.id.confirm_create);

        editTextActivityTime.setFocusable(false);
        editTextActivityTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        activityTimeHour = hour;
                        activityTimeMinute = minute;
                        isTimeSet = true;
                        String DateTime = activityTimeYear + "-" + String.format("%02d", activityTimeMonth) + "-" + String.format("%02d", activityTimeDay) + " " + String.format("%02d", activityTimeHour) + " : " + String.format("%02d", activityTimeMinute);
                        editTextActivityTime.setText(DateTime);
                    }
                },0,0,true).show();

                new DatePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    // 绑定监听器(How the parent is notified that the date is set.)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // 此处得到选择的时间，可以进行你想要的操作
                        activityTimeYear = year;
                        activityTimeMonth = monthOfYear + 1;
                        activityTimeDay = dayOfMonth;
                    }
                },
                        // 设置初始日期
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        editTextActivityEndTime.setFocusable(false);
        editTextActivityEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        activityEndTimeHour = hour;
                        activityEndTimeMinute = minute;
                        isEndTimeSet = true;
                        String DateTime = activityEndTimeYear + "-" + String.format("%02d", activityEndTimeMonth) + "-" + String.format("%02d", activityEndTimeDay) + " " + String.format("%02d", activityEndTimeHour) + " : " + String.format("%02d", activityEndTimeMinute);
                        editTextActivityEndTime.setText(DateTime);
                    }
                },0,0,true).show();

                new DatePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    // 绑定监听器(How the parent is notified that the date is set.)
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // 此处得到选择的时间，可以进行你想要的操作
                        activityEndTimeYear = year;
                        activityEndTimeMonth = monthOfYear + 1;
                        activityEndTimeDay = dayOfMonth;
                    }
                },
                        // 设置初始日期
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        editTextActivityDeadline.setFocusable(false);
        editTextActivityDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        activityDeadlineHour = hour;
                        activityDeadlineMinute = minute;
                        isDeadlineSet = true;
                        String DateTime = activityDeadlineYear + "-" + String.format("%02d", activityDeadlineMonth) + "-" + String.format("%02d", activityDeadlineDay) + " " + String.format("%02d", activityDeadlineHour) + " : " + String.format("%02d", activityDeadlineMinute);
                        editTextActivityDeadline.setText(DateTime);
                    }
                },0,0,true).show();

                new DatePickerDialog(CreateActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // 此处得到选择的时间，可以进行你想要的操作
                        activityDeadlineYear = year;
                        activityDeadlineMonth = monthOfYear + 1;
                        activityDeadlineDay = dayOfMonth;
                    }
                },
                        // 设置初始日期
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnRoadSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateActivity.this,MapSetActivity.class);
                startActivity(intent);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityName = ((EditText) findViewById(R.id.name_activity)).getText().toString().trim();
                activityIntro = ((EditText) findViewById(R.id.intro)).getText().toString().trim();
                activityAttention = ((EditText) findViewById(R.id.attention)).getText().toString().trim();
                if(activityName.length() == 0) {
                    Toast.makeText(CreateActivity.this,"活动名称不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isTimeSet) {
                    Toast.makeText(CreateActivity.this,"未设置开始时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isEndTimeSet) {
                    Toast.makeText(CreateActivity.this, "未设置结束时间", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isDeadlineSet) {
                    Toast.makeText(CreateActivity.this, "未设置报名截止时间", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editTextTeamMinPeople.getText().toString().length() == 0) {
                    Toast.makeText(CreateActivity.this, "未设置队伍最小人数", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    teamMinPeople = Integer.valueOf(editTextTeamMinPeople.getText().toString());
                }
                if (editTextTeamMaxPeople.getText().toString().length() == 0) {
                    Toast.makeText(CreateActivity.this, "未设置队伍最大人数", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    teamMaxPeople = Integer.valueOf(editTextTeamMaxPeople.getText().toString());
                }
                if (!timeCompare(activityTimeYear,activityTimeMonth,activityTimeDay,activityTimeHour,activityTimeMinute,activityEndTimeYear,activityEndTimeMonth,activityEndTimeDay,activityEndTimeHour,activityEndTimeMinute)) {
                    Toast.makeText(CreateActivity.this, "活动开始时间应早于活动结束时间", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!timeCompare(activityDeadlineYear,activityDeadlineMonth,activityDeadlineDay,activityDeadlineHour,activityDeadlineMinute,activityTimeYear,activityTimeMonth,activityTimeDay,activityTimeHour,activityTimeMinute)) {
                    Toast.makeText(CreateActivity.this, "报名截止时间应早于活动开始时间", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (teamMinPeople <= 0) {
                    Toast.makeText(CreateActivity.this, "每支队伍应至少有一人参赛", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (teamMaxPeople < teamMinPeople) {
                    Toast.makeText(CreateActivity.this, "每支队伍最小人数应小于最大人数", Toast.LENGTH_SHORT).show();
                    return;
                }

                confirm();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        latitudeList = (ArrayList<Double>) intent.getSerializableExtra("lat");
        longitudeList = (ArrayList<Double>) intent.getSerializableExtra("lng");
        typeList = intent.getIntegerArrayListExtra("type");
    }

    public void confirm() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                LocalSQLite localSQLite = new LocalSQLite(this,"UserInfo.db",null,1);

                json.put("user_id", localSQLite.getUid());
                json.put("activity_name",activityName);

                JSONObject timeJson = new JSONObject();
                timeJson.put("year", activityTimeYear);
                timeJson.put("month", activityTimeMonth);
                timeJson.put("day", activityTimeDay);
                timeJson.put("hour", activityTimeHour);
                timeJson.put("minute", activityTimeMinute);
                json.put("activity_time",timeJson);

                JSONObject endTimeJson = new JSONObject();
                endTimeJson.put("year", activityEndTimeYear);
                endTimeJson.put("month", activityEndTimeMonth);
                endTimeJson.put("day", activityEndTimeDay);
                endTimeJson.put("hour", activityEndTimeHour);
                endTimeJson.put("minute", activityEndTimeMinute);
                json.put("activity_end_time",endTimeJson);

                JSONObject deadlineJson = new JSONObject();
                deadlineJson.put("year", activityDeadlineYear);
                deadlineJson.put("month", activityDeadlineMonth);
                deadlineJson.put("day", activityDeadlineDay);
                deadlineJson.put("hour", activityDeadlineHour);
                deadlineJson.put("minute", activityDeadlineMinute);
                json.put("activity_registration_deadline",deadlineJson);

                json.put("min_people_in_team",teamMinPeople);
                json.put("max_people_in_team",teamMaxPeople);

                json.put("activity_intro",activityIntro);
                json.put("activity_attention",activityAttention);

                JSONArray keyPointList = new JSONArray();
                for(int i = 0; i < latitudeList.size(); i++)
                {
                    JSONObject keyPointJson = new JSONObject();
                    keyPointJson.put("latitude",latitudeList.get(i));
                    keyPointJson.put("longitude",longitudeList.get(i));
                    keyPointJson.put("type",typeList.get(i));
                    keyPointList.put(keyPointJson);
                }
                json.put("keypoint_list",keyPointList);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/create_activity") //创建活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CreateActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getInt("status") == 0) {
                                    Toast.makeText(CreateActivity.this, "创建成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else if (jsonObject.getInt("status") == 1) {
                                    Toast.makeText(CreateActivity.this,"创建失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    boolean timeCompare(Integer year1,Integer month1,Integer day1,Integer hour1,Integer minute1,Integer year2,Integer month2,Integer day2,Integer hour2,Integer minute2) {
        if (year1 < year2)
            return true;
        else if (year1 > year2)
            return false;
        else {
            if (month1 < month2)
                return true;
            else if (month1 > month2)
                return false;
            else {
                if (day1 < day2)
                    return true;
                else if (day1 > day2)
                    return false;
                else {
                    if (hour1 < hour2)
                        return true;
                    else if (hour1 > hour2)
                        return false;
                    else {
                        if (minute1 < minute2)
                            return true;
                        else if (minute1 > minute2)
                            return false;
                        else
                            return false;
                    }
                }
            }
        }
    }
}