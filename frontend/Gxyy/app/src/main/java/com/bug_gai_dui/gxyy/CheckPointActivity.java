package com.bug_gai_dui.gxyy;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CheckPointActivity extends AppCompatActivity {
    private Context context = this;
    private View.OnClickListener ivGoToChildClickListener;
    private int CheckPointId=1;
    private int ActivityId=123;
    private String ActivityTitle="越野202";

    int UserId=1003;
    String UserName="";

    public class TeamInfo{
        String TeamName="a";
        int TeamId=0;
        int MemberNum=0;
        int CheckedNum=0;
        long SpendTime=0;
    };
    public class MemberDetail{
        int UserId;
        String UserName;
        Boolean HadChecked;
        long CheckTime;
    };
    private List<List<MemberDetail>>TeamMember=new ArrayList<>();
    private List<TeamInfo> Teams=new ArrayList<>();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_point);
        AtyContainer.getInstance().addActivity(this);
        //获取基本信息
        ActivityTitle=getIntent().getStringExtra("ActivityTitle");
        CheckPointId=getIntent().getIntExtra("CheckPointId",1);
        ActivityId=getIntent().getIntExtra("ActivityId",1);

        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);

        UserName=dbhelper.getUserName();
        UserId=dbhelper.getUid();
        AskForCheckPointDetail();


    }
    private void init() throws JSONException {
        final ExpandableListView elv01 = findViewById(R.id.expandable);



 //自定义 展开/收起  图标的点击事件。position和 isExpand 都是通过tag 传递的
        ivGoToChildClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取被点击图标所在的group的索引
                Map<String, Object> map = (Map<String, Object>) v.getTag();
                int groupPosition = (int) map.get("groupPosition");
                Log.d("groupPosition", "onClick: groupPosition");
//                boolean isExpand = (boolean) map.get("isExpanded");   //这种是通过tag传值
                boolean isExpand = elv01.isGroupExpanded(groupPosition);    //判断分组是否展开

                if (isExpand) {
                    elv01.collapseGroup(groupPosition);
                } else {
                    elv01.expandGroup(groupPosition);
                }
            }
        };

        //创建并设置适配器
        MyExpandableListAdapter adapter = new MyExpandableListAdapter(Teams, TeamMember, this,
                ivGoToChildClickListener);
        Log.d("elv01", "init:"+(adapter==null?1:0));
        assert elv01 != null;
        elv01.setAdapter(adapter);

        //默认展开第一个分组
        //elv01.expandGroup(0);

        //展开某个分组时，并关闭其他分组。注意这里设置的是 ExpandListener
        elv01.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                //遍历 group 的数组（或集合），判断当前被点击的位置与哪个组索引一致，不一致就合并起来。
//                for (int i = 0; i < classes.length; i++) {
//                    if (i != groupPosition) {
//                        elv01.collapseGroup(i); //收起某个指定的组
//                    }
//                }
            }
        });

        //点击某个分组时，跳转到指定Activity
        elv01.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
               // Toast.makeText(CheckPointActivity.this, "组被点击了，跳转到具体的Activity", Toast.LENGTH_SHORT).show();
                return false;    //拦截点击事件，不再处理展开或者收起
            }
        });

        //某个分组中的子View被点击时的事件
        elv01.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition,
                                        long id) {
                return false;
            }
        });

    }

    private void AskForCheckPointDetail() {
        Thread ask=new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient okHttpClient = new OkHttpClient();

                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                JSONObject json = new JSONObject();
                try {
                    json.put("ActivityId", ActivityId);
                    json.put("CheckPointId", CheckPointId);
                    json.put("UserId", UserId);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/CheckPointDetail")
                        .post(requestBody)
                        .build();

                Call call = okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CheckPointActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        String res = Objects.requireNonNull(response.body()).string();
                        //解析传回的JSON
                        JSONObject jObject = null;
                        try {
                            JSONObject aStruct = null;
                            jObject = new JSONObject(res);
                            JSONArray arr1 = jObject.getJSONArray("TeamArray");
                            //JSONArray arr2=jObject.getJSONArray("Details");
                            for (int i = 0; i < arr1.length(); i++) {
                                aStruct = arr1.getJSONObject(i);//得到数组中对应下标对应的json对象
                                TeamInfo obj = new TeamInfo();
                                obj.CheckedNum = aStruct.getInt("CheckedNum");
                                obj.MemberNum = aStruct.getInt("MemberNum");

                                obj.TeamId = aStruct.getInt("TeamId");
                                obj.TeamName = aStruct.getString("TeamName");
                                Teams.add(obj);
                                JSONArray Members = aStruct.getJSONArray("Members");
                                List<MemberDetail> MemberOfATeam = new ArrayList<>();
                                for (int j = 0; j < Members.length(); j++) {
                                    aStruct = Members.getJSONObject(j);
                                    MemberDetail Member = new MemberDetail();
                                    Member.CheckTime = aStruct.getLong("CheckTime");
                                    Member.HadChecked = aStruct.getBoolean("HadChecked");
                                    Member.UserId = aStruct.getInt("UserId");
                                    Member.UserName = aStruct.getString("UserName");
                                    MemberOfATeam.add(Member);
                                }
                                TeamMember.add(MemberOfATeam);
                            }
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    init();
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                                Toast.makeText(CheckPointActivity.this, "获取打卡点信息成功", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });
        ask.start();
        try{
            ask.join();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

};