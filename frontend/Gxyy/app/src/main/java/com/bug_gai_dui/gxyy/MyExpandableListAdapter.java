package com.bug_gai_dui.gxyy;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CnPeng on 2017/1/21.
 * <p>
 * 自定义ExpandableListAdapter展示ExpandableListView的内容
 */

public class MyExpandableListAdapter extends BaseExpandableListAdapter {
    private List<List<CheckPointActivity.MemberDetail>>Item;
    private List<CheckPointActivity.TeamInfo> Group;
    private Context context;
    View.OnClickListener ivGoToChildClickListener;


    public MyExpandableListAdapter(List<CheckPointActivity.TeamInfo> Group, List<List<CheckPointActivity.MemberDetail>>Item, Context context,
                                   View.OnClickListener ivGoToChildClickListener) {
        this.Group = Group;
        this.Item = Item;
        this.context = context;
        this.ivGoToChildClickListener = ivGoToChildClickListener;
    }

    @Override
    public int getGroupCount() {    //组的数量
        return Group.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {    //某组中子项数量
        if(groupPosition<Item.size())
            return Item.get(groupPosition).size();
        else return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {     //某组
        return Group.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {  //某子项
        return Item.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupHold groupHold;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_elv_group, null);
            groupHold = new GroupHold();
            groupHold.tvGroupName = (TextView) convertView.findViewById(R.id.tv_groupName);
            groupHold.tvCheckInNum=(TextView)convertView.findViewById(R.id.tv_CheckInNum);
            convertView.setTag(groupHold);

        } else {
            groupHold = (GroupHold) convertView.getTag();

        }
        String groupName = Group.get(groupPosition).TeamName;
        groupHold.tvGroupName.setText(groupName);
        groupHold.tvCheckInNum.setText("打卡数："+String.valueOf(Group.get(groupPosition).CheckedNum) );
        //setTag() 方法接收的类型是object ，所以可将position和converView先封装在Map中。Bundle中无法封装view,所以不用bundle
        Map<String, Object> tagMap = new HashMap<>();
        tagMap.put("groupPosition", groupPosition);
        tagMap.put("isExpanded", isExpanded);
        //图标的点击事件
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        ChildHold childHold;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_elv_child, null);
            childHold = new ChildHold();
            childHold.tvChildName = (TextView) convertView.findViewById(R.id.tv_elv_childName);
            childHold.tvCheckInTime= (TextView) convertView.findViewById(R.id.tv_CheckTime);
            convertView.setTag(childHold);
        } else {
            childHold = (ChildHold) convertView.getTag();

        }

        String childName = Item.get(groupPosition).get(childPosition).UserName;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String checkTime=Item.get(groupPosition).get(childPosition).HadChecked?format0.format(Item.get(groupPosition).get(childPosition).CheckTime):"暂未打卡";
        childHold.tvChildName.setText(childName);
        childHold.tvCheckInTime.setText(checkTime);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;    //默认返回false,改成true表示组中的子条目可以被点击选中
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    class GroupHold {
        TextView  tvGroupName;
        TextView  tvCheckInNum;
    }

    class ChildHold {
        TextView tvChildName;
        TextView  tvCheckInTime;
    }
}