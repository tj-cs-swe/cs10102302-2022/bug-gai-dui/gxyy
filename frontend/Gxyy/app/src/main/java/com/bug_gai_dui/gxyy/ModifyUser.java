package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.bug_gai_dui.gxyy.ui.notifications.NotificationsFragment;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

//建了一些接口 ，目前author wyh
public class ModifyUser extends AppCompatActivity {

    String username;
    Integer userid;
    Integer select_sex_ig;
    String select_email_ig,select_name_ig,select_stu_no_ig;
    TextView modify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_user);
        ImageView backHome=findViewById(R.id.iv_backward);
        modify=(TextView) this.findViewById(R.id.tv_forward);
        backHome.setFocusable(false);
        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
        dbhelper.getUserName(); //返回最后登录的用户的用户名
        AtyContainer.getInstance().addActivity(this);
        userid=dbhelper.getUid();


        backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }});
        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ModifyUser.this, EditActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onStart() {
        getUserInfo();
        super.onStart();
    }
    public void getUserInfo() {
        new Thread(()->{

                // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                JSONObject json = new JSONObject();
                try {
                    json.put("uid", userid);

                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                //申明给服务端传递一个json串
                //创建一个OkHttpClient对象
                OkHttpClient okHttpClient = new OkHttpClient();
                //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
                //json为String类型的json数据
                RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
                //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
                Request request = new Request.Builder()
                        .url(getResources().getString(R.string.sever)+"/user_context")
                        .post(requestBody)
                        .build();

                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ModifyUser.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                    }

                                });
                            }
                        });   }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                String string = null;
                                try {
                                    string = response.body().string();
                                } catch (IOException e) {
                                    Toast.makeText(ModifyUser.this, "网络波动，请休息一下再试", Toast.LENGTH_SHORT).show();
                                }
                                //Log.i("info", string + "");
                                try {
                                    if(string!=null) {
                                        JSONObject jsonO = new JSONObject(string);
                                        username = jsonO.getString("username");

                                        select_sex_ig = jsonO.getInt("sex");
                                        select_email_ig = jsonO.getString("email");
                                        select_name_ig = jsonO.getString("name");
                                        select_stu_no_ig = jsonO.getString("student No");
                                        ItemGroup user, sex, email, uidItem, stuno, userName;

                                        user = findViewById(R.id.name_ig);
                                        sex = findViewById(R.id.select_sex_ig);
                                        email = findViewById(R.id.select_email_ig);
                                        uidItem = findViewById(R.id.id_user_ig);
                                        stuno = findViewById(R.id.select_stu_no_ig);
                                        userName = findViewById(R.id.select_name_ig);

                                        user.setContextTitle(username.toString());
                                        if (select_sex_ig == 0) {
                                            sex.setContextTitle("待设置");
                                        } else if (select_sex_ig == 1) {
                                            sex.setContextTitle("男");
                                        } else {
                                            sex.setContextTitle("女");
                                        }

                                        email.setContextTitle(select_email_ig);
                                        uidItem.setContextTitle(userid.toString());
                                        stuno.setContextTitle(select_stu_no_ig);
                                        userName.setContextTitle(select_name_ig);
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                    });
                    }
                });


        }).start();
    }
}