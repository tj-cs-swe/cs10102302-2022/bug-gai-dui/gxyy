package com.bug_gai_dui.gxyy;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class BottomActivity extends AppCompatActivity {

    boolean stopApp = false;
    List<String> mPermissionList = new ArrayList<>();
    String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int PERMISSION_REQUEST = 1;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom);
        AtyContainer.getInstance().addActivity(this);

        Thread check = new Thread(new Runnable() {
            @Override
            public void run() {
                checkPermission();
            }
        });
        check.start();
        try {
            check.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                finish();
                return;
            }
        }
        LocalSQLite localSQLite= new LocalSQLite(this,"UserInfo.db",null,1);
        if (localSQLite.empty()) {
            Intent start_intent = new Intent(BottomActivity.this, LoginActivity.class);
            startActivity(start_intent);
        } else {
            Intent start_intent = new Intent(BottomActivity.this, NavActivity.class);
            startActivity(start_intent);
        }
    }

    // 检查权限
    private void checkPermission() {
        mPermissionList.clear();
        //判断哪些权限未授予
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }
        /**
         * 判断是否为空
         */
        if (mPermissionList.isEmpty()) { //未授予的权限为空，表示都授予了
            LocalSQLite localSQLite= new LocalSQLite(this,"UserInfo.db",null,1);
            if (localSQLite.empty()) {
                Intent start_intent = new Intent(BottomActivity.this, LoginActivity.class);
                startActivity(start_intent);
            } else {
                Intent start_intent = new Intent(BottomActivity.this, NavActivity.class);
                startActivity(start_intent);
            }
        } else { //请求权限方法
            String[] permissions = mPermissionList.toArray(new String[mPermissionList.size()]);//将List转为数组
            ActivityCompat.requestPermissions(BottomActivity.this, permissions, PERMISSION_REQUEST);
        }
    }

}