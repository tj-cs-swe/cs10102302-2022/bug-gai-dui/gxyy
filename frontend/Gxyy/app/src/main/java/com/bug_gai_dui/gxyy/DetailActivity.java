package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class DetailActivity extends AppCompatActivity {

    Integer userId;
    String userName;
    Integer activityId;
    String activityCreator;
    String activityName;
    String activityIntro;
    String activityAttention;
    Integer activityTimeYear;
    Integer activityTimeMonth;
    Integer activityTimeDay;
    Integer activityTimeHour;
    Integer activityTimeMinute;
    Integer activityRegistrationDeadlineYear;
    Integer activityRegistrationDeadlineMonth;
    Integer activityRegistrationDeadlineDay;
    Integer activityRegistrationDeadlineHour;
    Integer activityRegistrationDeadlineMinute;
    Boolean isRegistered;
    Boolean isFavorite;
    Boolean isGM;
    Integer auditStatus;
    Boolean canSignUp;

    TextView textView_activity_name;
    TextView textView_activity_creator;
    TextView textView_activity_time;
    TextView textView_activity_intro;
    TextView textView_activity_attention;
    TextView textView_activity_registration_deadline;
    Button button_register;
    Button button_add_favorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        AtyContainer.getInstance().addActivity(this);
        Intent intent = getIntent();
        activityId = intent.getIntExtra("activity_id",0);

        LocalSQLite localSQLite = new LocalSQLite(DetailActivity.this,"UserInfo.db",null,1);
        userId = localSQLite.getUid();
        userName = localSQLite.getUserName();

        textView_activity_name = (TextView) findViewById(R.id.detail_activity_name);
        textView_activity_creator = (TextView) findViewById(R.id.detail_activity_creator);
        textView_activity_time = (TextView) findViewById(R.id.detail_activity_time);
        textView_activity_intro = (TextView) findViewById(R.id.detail_activity_intro);
        textView_activity_attention = (TextView) findViewById(R.id.detail_activity_attention);
        textView_activity_registration_deadline = (TextView) findViewById(R.id.detail_activity_registration_deadline);

        button_register = (Button) findViewById(R.id.detail_button_register);
        button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Thread setActivityInfo = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivityInfo();
            }
        });
        setActivityInfo.start();
        try {
            setActivityInfo.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        textView_activity_name.setText(activityName);
        textView_activity_creator.setText(activityCreator);
        textView_activity_time.setText(activityTimeYear + "-" + String.format("%02d", activityTimeMonth) + "-" + String.format("%02d", activityTimeDay) + " " + String.format("%02d", activityTimeHour) + " : " + String.format("%02d", activityTimeMinute));
        textView_activity_intro.setText(activityIntro);
        textView_activity_attention.setText(activityAttention);
        textView_activity_registration_deadline.setText(activityRegistrationDeadlineYear + "-" + String.format("%02d", activityRegistrationDeadlineMonth) + "-" + String.format("%02d", activityRegistrationDeadlineDay) + " " + String.format("%02d", activityRegistrationDeadlineHour) + " : " + String.format("%02d", activityRegistrationDeadlineMinute));

        Thread setIdentity = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("run", "设置权限: 子线程开始");
                getIdentity();
                Log.d("run", "设置权限: 子线程结束");
            }
        });
        setIdentity.start();
        try {
            setIdentity.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (!isGM) {
            Thread getActivityRelation = new Thread(new Runnable() {
                @Override
                public void run() {
                    getRelation();
                }
            });
            getActivityRelation.start();
            try {
                getActivityRelation.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        if (!isGM) {
            if (isFavorite) {
                button_add_favorite.setText("已收藏");
            } else {
                button_add_favorite.setText("收藏");
            }
            button_add_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isFavorite) {
                        deleteFavorite();
                    } else {
                        addFavorite();
                    }
                }
            });

            if (isRegistered) {
                button_register.setText("已报名");
                button_register.setClickable(false);
            } else {
                if (canSignUp) {
                    button_register.setText("报名");
                    button_register.setClickable(true);
                    button_register.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final Integer[] status = new Integer[1];
                            Thread checkLeader = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    OkHttpClient okHttpClient = new OkHttpClient();

                                    MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                                    JSONObject json = new JSONObject();
                                    try {
                                        json.put("activity_id", activityId);
                                        json.put("user_name", userName);
                                    } catch (JSONException e) {
                                        throw new RuntimeException(e);
                                    }

                                    RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                                    Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/check_registered") //获取活动信息接口
                                            .post(requestBody)
                                            .build();

                                    Call call = okHttpClient.newCall(request);
                                    Response response = null;
                                    try {
                                        response = call.execute();
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                    if (response.isSuccessful()) {
                                        String res = null;
                                        try {
                                            res = Objects.requireNonNull(response.body()).string();
                                        } catch (IOException e) {
                                            throw new RuntimeException(e);
                                        }
                                        try {
                                            JSONObject jsonObject = new JSONObject(res);
                                            status[0] = jsonObject.getInt("status");
                                        } catch (JSONException e) {
                                            Toast.makeText(DetailActivity.this, "网络波动，请重试", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                            checkLeader.start();
                            try {
                                checkLeader.join();
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                            if (status[0] == 2) {
                                Intent intent = new Intent(DetailActivity.this, SignUpActivity.class);
                                intent.putExtra("activity_id",activityId);
                                startActivity(intent);
                            } else if (status[0] == 3) {
                                Toast.makeText(DetailActivity.this, "此时间段已报名其他活动", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    button_register.setText("报名时间已过");
                    button_register.setClickable(false);
                }
            }

        } else {
            if (auditStatus == 0) {
                button_register.setText("通过");
                button_register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        auditActivity(true);
                    }
                });
                button_add_favorite.setText("不通过");
                button_add_favorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        auditActivity(false);
                    }
                });
            } else {
                button_register.setText("审核已完成");
                button_register.setClickable(false);
                if (auditStatus == 1) {
                    button_add_favorite.setText("审核通过");
                    button_add_favorite.setClickable(false);
                } else if (auditStatus == 2) {
                    button_add_favorite.setText("审核不通过");
                    button_add_favorite.setClickable(false);
                }
            }
        }
    }

    void getActivityInfo() {
        OkHttpClient okHttpClient = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json;charset=utf-8");
        JSONObject json = new JSONObject();
        try {
            json.put("activity_id", activityId);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
        Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/activity_info") //获取活动信息接口
                .post(requestBody)
                .build();

        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if(response.isSuccessful()) {
            String res = null;
            try {
                res = Objects.requireNonNull(response.body()).string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                JSONObject jsonObject = new JSONObject(res);
                activityCreator = jsonObject.getString("creator_name");
                activityName = jsonObject.getString("activity_name");
                activityTimeYear = jsonObject.getJSONObject("activity_time").getInt("year");
                activityTimeMonth = jsonObject.getJSONObject("activity_time").getInt("month");
                activityTimeDay = jsonObject.getJSONObject("activity_time").getInt("day");
                activityTimeHour = jsonObject.getJSONObject("activity_time").getInt("hour");
                activityTimeMinute = jsonObject.getJSONObject("activity_time").getInt("minute");
                activityRegistrationDeadlineYear = jsonObject.getJSONObject("activity_registration_deadline").getInt("year");
                activityRegistrationDeadlineMonth = jsonObject.getJSONObject("activity_registration_deadline").getInt("month");
                activityRegistrationDeadlineDay = jsonObject.getJSONObject("activity_registration_deadline").getInt("day");
                activityRegistrationDeadlineHour = jsonObject.getJSONObject("activity_registration_deadline").getInt("hour");
                activityRegistrationDeadlineMinute = jsonObject.getJSONObject("activity_registration_deadline").getInt("minute");
                activityIntro = jsonObject.getString("activity_intro");
                activityAttention = jsonObject.getString("activity_attention");
                auditStatus = jsonObject.getInt("audit_status");
                canSignUp = jsonObject.getBoolean("can_sign_up");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    void getRelation() {
        OkHttpClient okHttpClient = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json;charset=utf-8");
        JSONObject json = new JSONObject();
        try {
            json.put("user_id", userId);
            json.put("activity_id", activityId);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
        Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/check_relation")
                .post(requestBody)
                .build();

        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (response.isSuccessful()) {
            String res = null;
            try {
                res = Objects.requireNonNull(response.body()).string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                JSONObject jsonObject = new JSONObject(res);
                isRegistered = jsonObject.getBoolean("is_registered");
                isFavorite = jsonObject.getBoolean("is_favorite");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    void getIdentity() {
        OkHttpClient okHttpClient = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json;charset=utf-8");
        JSONObject json = new JSONObject();
        try {
            json.put("uid", userId);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
        Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/user_context")
                .post(requestBody)
                .build();

        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (response.isSuccessful()) {
            String res = null;
            try {
                res = Objects.requireNonNull(response.body()).string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                JSONObject jsonObject = new JSONObject(res);
                Log.d("run", "设置isGM");
                isGM = jsonObject.getBoolean("isGM");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

    void addFavorite() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", userId);
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/add_favorite") //收藏活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                int status = jsonObject.getInt("status");

                                Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
                                if(status == 0) {
                                    button_add_favorite.setText("已收藏");
                                    Toast.makeText(DetailActivity.this,"收藏成功",Toast.LENGTH_SHORT).show();
                                    isFavorite = true;
                                } else if(status == 1) {
                                    button_add_favorite.setText("已收藏");
                                    Toast.makeText(DetailActivity.this,"已收藏",Toast.LENGTH_SHORT).show();
                                    isFavorite = true;
                                } else if(status == 2) {
                                    Toast.makeText(DetailActivity.this, "收藏失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void deleteFavorite() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", userId);
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/delete_favorite") //收藏活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                int status = jsonObject.getInt("status");

                                Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
                                if(status == 0) {
                                    button_add_favorite.setText("收藏");
                                    Toast.makeText(DetailActivity.this,"取消收藏成功",Toast.LENGTH_SHORT).show();
                                    isFavorite = false;
                                } else if(status == 1) {
                                    button_add_favorite.setText("收藏");
                                    Toast.makeText(DetailActivity.this,"未收藏",Toast.LENGTH_SHORT).show();
                                    isFavorite = false;
                                } else if(status == 2) {
                                    Toast.makeText(DetailActivity.this, "取消收藏失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void auditActivity(boolean result) {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("activity_id", activityId);
                json.put("result",result);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/audit_activity")
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                if (jsonObject.getInt("status") == 0) {
                                    Toast.makeText(DetailActivity.this, "操作成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else if (jsonObject.getInt("status") == 1) {
                                    Toast.makeText(DetailActivity.this, "该活动已被审核", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }
}