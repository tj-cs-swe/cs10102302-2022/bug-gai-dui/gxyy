package com.bug_gai_dui.gxyy.ui.notifications;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import com.bug_gai_dui.gxyy.*;
import com.bug_gai_dui.gxyy.CircleImage.XCRoundImageView;
import com.bug_gai_dui.gxyy.databinding.FragmentNotificationsBinding;

import static com.bug_gai_dui.gxyy.NavActivity.isGM;
import static com.bug_gai_dui.gxyy.NavActivity.sex;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificationsBinding binding;
    private View view;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        notificationsViewModel =
//                new ViewModelProvider(this).get(NotificationsViewModel.class);
//        final TextView textView = binding.textNotifications;
//        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        //建了一些接口 ，目前author wyh
        view = inflater.inflate(R.layout.fragment_notifications, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_2);
        toolbar.setTitle("个人中心");//设置主标题名称
        toolbar.setBackgroundColor(Color.argb(0,111,183,107));
        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        String name;
        TextView name_set;
        LocalSQLite dbhelper= new LocalSQLite(getActivity(),"UserInfo.db",null,1);//第一步创建数据库帮助类
        dbhelper.getUserName();//返回最后登录的用户的用户名
        name_set=root.findViewById(R.id.get_user_name);
        name=dbhelper.getUserName();
        name_set.setText(name);




        //查看个人详细信息的页面跳转
        LinearLayout gethistory=root.findViewById(R.id.user_home_head);
        gethistory.setFocusable(false);
        gethistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ModifyUser.class);
                startActivity(intent);
            }});

        //查看历史记录的页面跳转
        LinearLayout getUserHead=root.findViewById(R.id.get_user_history);
        getUserHead.setFocusable(false);
        getUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                startActivity(intent);
            }});

        //已收藏的页面跳转
        LinearLayout haveStore=root.findViewById(R.id.get_user_have_store);

        haveStore.setFocusable(false);
        haveStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CollectActivity.class);
                startActivity(intent);
            }});
        //已发布的页面跳转
        LinearLayout havePost=root.findViewById(R.id.get_user_have_post);

        havePost.setFocusable(false);
        havePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PublishedActivity.class);
                startActivity(intent);
            }});
        LinearLayout havebaoming=root.findViewById(R.id.get_user_have_baoming);

        havebaoming.setFocusable(false);
        havebaoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignedActivity.class);
                startActivity(intent);
            }});
        LinearLayout needProcess=root.findViewById(R.id.get_user_need_process);

        needProcess.setFocusable(false);
        needProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UnauditActivity.class);
                startActivity(intent);
            }});

        NavActivity navActivity=(NavActivity) getActivity();
        XCRoundImageView touxiang= root.findViewById(R.id.touxiang_rechange);
        LinearLayout need_hide=root.findViewById(R.id.useless_to_tongyi);
        TextView email_Change=root.findViewById(R.id.get_user_email_name);
        navActivity.getUserInfo(touxiang,need_hide,needProcess,email_Change);






        Button button_login_out = (Button) root.findViewById(R.id.button_login_out);
        button_login_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalSQLite localSQLite = new LocalSQLite(getActivity(),"UserInfo.db",null,1);
                AtyContainer.getInstance().finishAllActivity();
                localSQLite.clear();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        return root;
        }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}