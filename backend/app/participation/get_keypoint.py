from flask import request
from . import participation
from ..models import Activity
import time

@participation.route('/KeyPoint', methods = ['GET', 'POST'])
def get_keypoint():
    ret = {}    
    if request.method == 'POST':
        context = request.get_json()
        activity_id = context['ActivityId']
        activity = Activity.query.filter_by(id = int(activity_id)).first()
        check = []
        supply = []
        for keypoint in activity.keypoint:
            point_info = {}
            point_info['latitude'] = float(keypoint.latitude)
            point_info['longitude'] = float(keypoint.longitude)
            if keypoint.type == 0:
                ret['Begin'] = point_info
            elif keypoint.type == 1:
                ret['Destination'] = point_info
            elif keypoint.type == 2:
                point_info['id'] = keypoint.id
                check.append(point_info)
            elif keypoint.type == 3:
                point_info['id'] = keypoint.id
                supply.append(point_info)
        ret['CheckInInfo'] = check
        ret['SupplyInfo'] = supply

        time_array = time.strptime(str(activity.start_time), "%Y-%m-%d %H:%M:%S")
        time_stamp = time.mktime(time_array)
        ret['TimeStart'] = time_stamp
        time_array = time.strptime(str(activity.end_time), "%Y-%m-%d %H:%M:%S")
        time_stamp = time.mktime(time_array)
        ret['TimeEnd'] = time_stamp
    return ret