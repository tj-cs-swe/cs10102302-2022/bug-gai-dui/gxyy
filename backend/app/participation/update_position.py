from flask import request
from .. import db
from . import participation
from ..models import User, Activity

@participation.route('/UpdatePosition', methods = ['GET', 'POST'])
def update_position():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        user = User.query.filter_by(id = int(context['UserId'])).first()
        activity = Activity.query.filter_by(id = int(context['ActivityId'])).first()
        if user.role == 1:
            teammates = []   
            for team in activity.team:
                for member in team.members:
                    teammate_info = {}
                    teammate_info['id'] = member.id
                    teammate_info['name'] = member.name
                    teammate_position = next(i for i in member.positions if i.activity_id == int(context['ActivityId']))
                    teammate_info['latitude'] = teammate_position.latitude
                    teammate_info['longitude'] = teammate_position.longitude
                    teammate_info['TeamName'] = team.leader.name + '的队伍'
                    teammate_info['TeamId'] = team.id                
                    teammates.append(teammate_info)
            ret['TeammateInfo'] = teammates
        else:
            new_position = context['UserPosition']
            position = next(i for i in user.positions if i.activity_id == int(context['ActivityId']))
            position.latitude = new_position['latitude']
            position.longitude = new_position['longitude']
            db.session.commit()
            team = next(i for i in user.teams if i.activity_id == int(context['ActivityId']))
            teammates = []                
            for member in team.members:
                if member.id != user.id:
                    teammate_info = {}
                    teammate_info['id'] = member.id
                    teammate_info['name'] = member.name
                    teammate_position = next(i for i in member.positions if i.activity_id == int(context['ActivityId']))
                    teammate_info['latitude'] = teammate_position.latitude
                    teammate_info['longitude'] = teammate_position.longitude
                    teammate_info['TeamName'] = team.leader.name + '的队伍'
                    teammate_info['TeamId'] = team.id                
                    teammates.append(teammate_info)
            ret['TeammateInfo'] = teammates

    return ret
