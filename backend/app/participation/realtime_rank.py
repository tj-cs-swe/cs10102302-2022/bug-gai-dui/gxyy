from flask import request
from . import participation
from ..models import Activity, Team, Keypoint, user_point, User
from sqlalchemy import and_

@participation.route('/RealtimeRank', methods = ['GET', 'POST'])
def realtime_rank():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        activity = Activity.query.filter_by(id = int(context['ActivityId'])).first()
        teams = Team.query.filter_by(activity_id = activity.id).order_by(Team.point_count.desc()).order_by(Team.time_used)
        team_list = []
        for team in teams:
            team_info = {}
            team_info['Leader'] = team.leader.name
            team_info['PointCount'] = team.point_count
            team_info['TimeUsed'] = str(team.time_used - activity.start_time)
            member_list = []
            for member in team.members:
                member_info = {}
                member_info['UserName'] = member.name
                member_info['Count'] = Keypoint.query.join(user_point).join(User) \
                                               .filter(and_(Keypoint.activity_id == activity.id, User.id == member.id))\
                                               .count()
                member_list.append(member_info)
            team_info['Members'] = member_list
            team_list.append(team_info)
        ret['TeamList'] = team_list
    return ret
