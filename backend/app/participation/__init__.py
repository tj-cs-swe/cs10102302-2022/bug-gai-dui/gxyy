from flask import Blueprint

participation = Blueprint('participation', __name__)

from . import check_in
from . import get_keypoint
from . import update_position
from . import realtime_rank
from . import check_point_detail