from flask import request
from .. import db
from . import participation
from ..models import User, Keypoint, CheckInInfo
from datetime import datetime

@participation.route('/CheckIn', methods = ['GET', 'POST'])
def check_in():
    if request.method == 'POST':
        context = request.get_json()
        user = User.query.filter_by(id = int(context['UserId'])).first()
        point = Keypoint.query.filter_by(id = int(context['CheckPointId'])).first()
        if point in user.checkpoints:
            return {'status': 2}
        user.checkpoints.append(point)

        new_check_in_info = CheckInInfo(time = datetime.now(), activity_id = point.activity_id, user_id = user.id, checkpoint_id = point.id)
        db.session.add(new_check_in_info)
        db.session.commit()

        team = next(i for i in user.teams if i.activity_id == int(context['ActivityId']))
        if point in team.checkpoints:
            return {'status': 0}
        else:
            team.checkpoints.append(point)
            team.point_count += 1
            team.time_used = datetime.now()
            db.session.commit()
            return {'status': 0}
    return {'status': 1}
