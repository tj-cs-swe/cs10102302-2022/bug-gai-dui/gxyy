from flask import request
from . import participation
from ..models import Activity, Keypoint, CheckInInfo
import time

@participation.route('/CheckPointDetail', methods = ['GET', 'POST'])
def check_point_detail():
    ret = {}    
    if request.method == 'POST':
        context = request.get_json()
        activity = Activity.query.filter_by(id = int(context['ActivityId'])).first()
        checkpoint = Keypoint.query.filter_by(id = int(context['CheckPointId'])).first()
        team_list = []
        for team in activity.team:
            team_info = {}
            team_info['TeamId'] = team.id
            team_info['TeamName'] = team.leader.name + "的队伍"
            team_info['MemberNum'] = len(team.members)
            team_info['CheckedNum'] = team.point_count
            member_list = []
            for member in team.members:
                member_info = {}
                member_info['UserName'] = member.name
                member_info['UserId'] = member.id
                if checkpoint in member.checkpoints:
                    member_info['HadChecked'] = True
                    check_in_info = CheckInInfo.query.filter_by(activity_id = activity.id, user_id = member.id, checkpoint_id = checkpoint.id).first()
                    time_array = time.strptime(str(check_in_info.time), "%Y-%m-%d %H:%M:%S")
                    time_stamp = int(time.mktime(time_array))
                    member_info['CheckTime'] = time_stamp
                else:
                    member_info['HadChecked'] = False
                    member_info['CheckTime'] = 0
                member_list.append(member_info)
            team_info['Members'] = member_list
            team_list.append(team_info)
        ret['TeamArray'] = team_list
    return ret
