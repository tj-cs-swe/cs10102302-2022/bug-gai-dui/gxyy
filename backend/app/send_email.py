from . import mail
from flask_mail import Message
from threading import Thread

def send_async_email(app, msg): 
    with app.app_context():
        mail.send(msg)

def send_email(to, title, context):
    msg = Message(title, sender='bug_fixed@163.com', recipients=[to])
    msg.body = context
    from server import app       
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr