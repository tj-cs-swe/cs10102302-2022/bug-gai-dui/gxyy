from . import db
from werkzeug.security import generate_password_hash, check_password_hash

# 关系表
favorites = db.Table('favorites',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('activity_id', db.Integer, db.ForeignKey('activities.id'))
)

register = db.Table('register',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('activity_id', db.Integer, db.ForeignKey('activities.id'))
)

team_member = db.Table('team_member',
    db.Column('team_id', db.Integer, db.ForeignKey('teams.id')), 
    db.Column('user_id', db.Integer, db.ForeignKey('users.id'))
)

team_point = db.Table('team_point',
    db.Column('team_id', db.Integer, db.ForeignKey('teams.id')),
    db.Column('checkpoint_id', db.Integer, db.ForeignKey('keypoints.id'))  
)

user_point = db.Table('user_point',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('checkpoint_id', db.Integer, db.ForeignKey('keypoints.id'))
)

class Verification(db.Model):
    # 定义表名
    __tablename__ = 'verifications'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    mail_address = db.Column(db.String(32), unique=False, index=True)
    verification_code = db.Column(db.String(4))
    send_time = db.Column(db.DateTime) # xxxx - xx - xx

class User(db.Model):
    # 定义表名
    __tablename__ = 'users'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    role = db.Column(db.Integer)
    name = db.Column(db.String(20), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    mail_address = db.Column(db.String(32), unique=True, index=True)
    sex = db.Column(db.Integer)
    accurate_name = db.Column(db.String(20))
    student_no = db.Column(db.String(20))

    # 建立多对多关系
    favorites = db.relationship('Activity', secondary=favorites, back_populates='favorite_users')
    activities = db.relationship('Activity', secondary=register, back_populates='register_users')
    teams = db.relationship('Team', secondary=team_member, back_populates='members')
    checkpoints = db.relationship('Keypoint', secondary=user_point, back_populates='users')
    # 一对多属性
    announced = db.relationship('Activity', uselist=True, backref="owner", lazy='dynamic')
    lead_teams = db.relationship('Team', uselist=True, backref="leader", lazy='dynamic')
    positions = db.relationship("Position", uselist=True, backref="user", lazy='dynamic')

    def set_password(self, password):  # 用来设置密码的方法，接受密码作为参数
        self.password_hash = generate_password_hash(password)  # 将生成的密码保持到对应字段

    def validate_password(self, password):  # 用于验证密码的方法，接受密码作为参数
        return check_password_hash(self.password_hash, password)  # 返回布尔值

class Activity(db.Model):
    # 定义表名
    __tablename__ = 'activities'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(30), index=True)
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    registration_deadline = db.Column(db.DateTime)
    intro = db.Column(db.String(256))
    attention = db.Column(db.String(256))
    minimum_members = db.Column(db.Integer)
    maximum_members = db.Column(db.Integer)
    auditing = db.Column(db.Integer) # 1表示审核中，0表示已发布
    # 一对多属性
    keypoint = db.relationship("Keypoint", uselist=True, backref="activity", lazy='dynamic')
    team = db.relationship("Team", uselist=True, backref="activity", lazy='dynamic')
    # 建立多对多关系
    favorite_users = db.relationship('User', secondary=favorites, back_populates='favorites')
    register_users = db.relationship('User', secondary=register, back_populates='activities')
    # 外键
    owner_id = db.Column(db.Integer, db.ForeignKey(User.id))

class Keypoint(db.Model):
    # 定义表名
    __tablename__ = 'keypoints'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    latitude = db.Column(db.DECIMAL(12,8))
    longitude = db.Column(db.DECIMAL(12,8))
    type = db.Column(db.Integer)
    # 外键
    activity_id = db.Column(db.Integer, db.ForeignKey(Activity.id))
    # 建立多对多关系
    teams = db.relationship('Team', secondary=team_point, back_populates='checkpoints')
    users = db.relationship('User', secondary=user_point, back_populates='checkpoints')

class Team(db.Model):
    # 定义表名
    __tablename__ = 'teams'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    point_count = db.Column(db.Integer)
    time_used = db.Column(db.DateTime)
    # 外键
    activity_id = db.Column(db.Integer, db.ForeignKey(Activity.id))
    leader_id = db.Column(db.Integer, db.ForeignKey(User.id))
    # 建立多对多关系
    members = db.relationship('User', secondary=team_member, back_populates='teams')
    checkpoints = db.relationship('Keypoint', secondary=team_point, back_populates='teams')

class Position(db.Model):
    # 定义表名
    __tablename__ = 'positions'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    latitude = db.Column(db.DECIMAL(12,8))
    longitude = db.Column(db.DECIMAL(12,8))
    activity_id = db.Column(db.Integer)
    # 外键
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))

class CheckInInfo(db.Model):
    # 定义表名
    __tablename__ = 'check_in_info'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    time = db.Column(db.DateTime)
    activity_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    checkpoint_id = db.Column(db.Integer)
