from flask import request
from . import activity
from ..models import User, Activity
from datetime import datetime

@activity.route('/activity_info', methods = ['GET', 'POST'])
def activity_info():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        id = context['activity_id']
        activity = Activity.query.filter_by(id = int(id)).first()
        user = User.query.filter_by(id = activity.owner_id).first()
        ret['creator_name'] = user.name
        ret['activity_name'] = activity.name
        activity_time = {}
        activity_time['year'] = activity.start_time.year
        activity_time['month'] = activity.start_time.month
        activity_time['day'] = activity.start_time.day
        activity_time['hour'] = activity.start_time.hour
        activity_time['minute'] = activity.start_time.minute
        ret['activity_time'] = activity_time
        activity_end_time = {}
        activity_end_time['year'] = activity.end_time.year
        activity_end_time['month'] = activity.end_time.month
        activity_end_time['day'] = activity.end_time.day
        activity_end_time['hour'] = activity.end_time.hour
        activity_end_time['minute'] = activity.end_time.minute
        ret['activity_endtime'] = activity_end_time
        activity_registration_deadline = {}
        activity_registration_deadline['year'] = activity.registration_deadline.year
        activity_registration_deadline['month'] = activity.registration_deadline.month
        activity_registration_deadline['day'] = activity.registration_deadline.day
        activity_registration_deadline['hour'] = activity.registration_deadline.hour
        activity_registration_deadline['minute'] = activity.registration_deadline.minute
        ret['activity_registration_deadline'] = activity_registration_deadline
        ret['min_people_in_team'] = activity.minimum_members
        ret['max_people_in_team'] = activity.maximum_members
        ret['activity_intro'] = activity.intro
        ret['activity_attention'] = activity.attention
        ret['num_of_register'] = len(activity.register_users)
        ret['num_of_favorite'] = len(activity.favorite_users)
        ret['audit_status'] = 1 - int(activity.auditing)
        ret['can_sign_up'] = bool(datetime.now() < activity.registration_deadline)
    return ret

