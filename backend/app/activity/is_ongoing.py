from flask import request
from . import activity
from ..models import User, Activity
from datetime import datetime
from sqlalchemy import and_

@activity.route('/is_ongoing', methods = ['GET', 'POST'])
def is_ongoing():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        user = User.query.filter_by(name = context['username']).first() 
        activities = Activity.query.filter(and_(Activity.start_time <= datetime.now(), Activity.end_time >= datetime.now()))
        for activity in activities:
            if activity in user.announced:
                ret['id'] = activity.id
                ret['activityname'] = activity.name
                ret['state'] = 1
                ret['status'] = 2
                return ret
            if activity in user.activities:
                ret['id'] = activity.id
                ret['activityname'] = activity.name
                ret['state'] = 1
                ret['status'] = 1
                return ret
        ret['id'] = 0
        ret['activityname'] = ''
        ret['status'] = 0
        ret['state'] = 0
    return ret


