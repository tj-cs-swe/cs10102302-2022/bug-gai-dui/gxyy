from flask import request
from .. import db
from . import activity
from ..models import User, Activity
import json

@activity.route('/delete_favorite', methods = ['GET', 'POST'])
def delete_favorite():
    if request.method == 'POST':
        context = request.get_json()
        user_id = context['user_id']
        activity_id = context['activity_id']
        user = User.query.filter_by(id = int(user_id)).first()
        activity = Activity.query.filter_by(id = int(activity_id)).first()        
        favorites = user.favorites
        # 已收藏
        if activity in favorites:
            user.favorites.remove(activity)
            db.session.commit()             
            return json.dumps({'status': 0})          
        # 未收藏
        return json.dumps({'status': 1})
    else:
        return json.dumps({'status': 2})