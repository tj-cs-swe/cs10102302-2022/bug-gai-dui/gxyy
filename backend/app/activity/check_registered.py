from flask import request
from . import activity
from ..models import User, Activity
from sqlalchemy import or_, and_

@activity.route('/check_registered', methods = ['GET', 'POST'])
def check_registered():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        users = User.query.filter_by(name = context['user_name']).all()
        activity = Activity.query.filter_by(id = int(context['activity_id'])).first()
        if len(users) == 0:
            ret['status'] = 0
            ret['id'] = 0
        else:
            user = users[0]
            ret['id'] = user.id
            if user.role:
                ret['status'] = 4
                return ret
            if activity in user.activities:
                ret['status'] = 1
                return ret
            conflict_activities = Activity.query.filter(or_(and_(Activity.start_time > activity.start_time, 
                                                                 Activity.start_time < activity.end_time),
                                                            and_(Activity.end_time > activity.start_time, 
                                                                 Activity.end_time < activity.end_time),
                                                            and_(Activity.start_time <= activity.start_time,
                                                                 Activity.end_time >= activity.end_time))).all()
            for conflict_activity in conflict_activities:
                if conflict_activity in user.activities:
                    ret['status'] = 3
                    return ret
            ret['status'] = 2
    return ret
