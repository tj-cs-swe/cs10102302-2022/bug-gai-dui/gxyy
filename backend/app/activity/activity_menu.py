from flask import request
from . import activity
from .. import db
from ..models import Activity
from datetime import date, datetime

@activity.route('/activity_menu', methods = ['GET', 'POST'])
def activity_menu():
    ret = {}
    if request.method == 'GET':
        menu = []
        activities = Activity.query.filter(Activity.end_time > datetime.now()).order_by(Activity.start_time).all()
        for activity in activities:
            if activity.auditing:
                continue
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.start_time.year
            activity_date['m'] = activity.start_time.month
            activity_date['d'] = activity.start_time.day
            info['date'] = activity_date
            if datetime.now() <= activity.registration_deadline:
                info['state'] = 0
            elif datetime.now() < activity.start_time:
                info['state'] = 1
            else:
                info['state'] = 2
            menu.append(info)
        ret['menu'] = menu
    return ret

