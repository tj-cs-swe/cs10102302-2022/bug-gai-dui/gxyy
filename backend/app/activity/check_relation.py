from flask import request
from . import activity
from ..models import User, Activity

@activity.route('/check_relation', methods = ['GET', 'POST'])
def check_relation():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        user_id = context['user_id']
        activity_id = context['activity_id']
        user = User.query.filter_by(id = int(user_id)).first()
        activity = Activity.query.filter_by(id = int(activity_id)).first()
        ret['is_registered'] = activity in user.activities
        ret['is_favorite'] = activity in user.favorites
    return ret