from flask import Blueprint

activity = Blueprint('activity', __name__)

from . import create_activity
from . import activity_menu
from . import activity_info
from . import check_relation
from . import check_registered
from . import register_activity
from . import add_favorite
from . import delete_favorite
from . import result_activity
from . import is_ongoing
from . import audit_activity