from flask import request
from . import activity
from .. import db
from ..models import Activity

@activity.route('/audit_activity', methods = ['GET', 'POST'])
def audit_activity():
    if request.method == 'POST':
        context = request.get_json()
        activity = Activity.query.filter_by(id = int(context['activity_id'])).first()
        if activity.auditing:
            if context['result'] == True:
                activity.auditing = 0
            else:
                db.session.delete(activity)
            db.session.commit()
            return {'status': 0}
    return {'status': 1}

