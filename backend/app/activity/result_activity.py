from flask import request
from . import activity
from ..models import Activity
from datetime import datetime
from sqlalchemy import or_

@activity.route('/result_activity', methods = ['GET', 'POST'])
def result_activity():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        keywords = context['searchText']
        keyword_list = keywords.split()
        for i in range(len(keyword_list)):
            keyword_list[i] = '%' + keyword_list[i] + '%'
        menu = []
        activities = Activity.query.filter(or_(*[Activity.name.like(w) for w in keyword_list])) \
                                   .filter(Activity.end_time > datetime.now()) \
                                   .order_by(Activity.start_time).all()
        for activity in activities:
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.start_time.year
            activity_date['m'] = activity.start_time.month
            activity_date['d'] = activity.start_time.day
            info['date'] = activity_date
            if datetime.now() <= activity.registration_deadline:
                info['state'] = 0
            elif datetime.now() < activity.start_time:
                info['state'] = 1
            else:
                info['state'] = 2
            menu.append(info)
        ret['menu'] = menu
    return ret

