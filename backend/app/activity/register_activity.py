from flask import request
from .. import db
from . import activity
from ..models import User, Team, Position, Activity, Keypoint
from datetime import datetime, time

@activity.route('/register_activity', methods = ['GET', 'POST'])
def register_activity():
    if request.method == 'POST':
        context = request.get_json()
        id_lst = context['member_id']
        leader = User.query.filter_by(id = int(context['captain_id'])).first()
        activity = Activity.query.filter_by(id = int(context['activity_id'])).first()
        new_team = Team(point_count = 0, time_used = datetime(activity.start_time.year, 
                                                              activity.start_time.month, 
                                                              activity.start_time.day, 
                                                              activity.start_time.hour, 
                                                              activity.start_time.minute, 
                                                              activity.start_time.second, ))  
        db.session.add(new_team)
        activity.team.append(new_team)

        start_point = Keypoint.query.filter_by(activity_id = activity.id, type = 0).first()

        leader.lead_teams.append(new_team)
        leader.activities.append(activity)
        new_position = Position(latitude = start_point.latitude, longitude = start_point.longitude, activity_id = activity.id)
        db.session.add(new_position)
        leader.positions.append(new_position)

        new_team.members.append(leader)

        for member_id in id_lst:
            member = User.query.filter_by(id = int(member_id)).first()
            new_team.members.append(member)
            member.activities.append(activity)            
            new_position = Position(latitude = start_point.latitude, longitude = start_point.longitude, activity_id = activity.id)
            db.session.add(new_position)
            member.positions.append(new_position)

        db.session.commit()
        return {'status': 0}
    else:
        return {'status': 1}
