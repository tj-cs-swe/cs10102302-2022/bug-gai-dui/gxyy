from flask import request
from . import activity
from .. import db
from ..models import User, Activity, Keypoint
from datetime import datetime
import json

@activity.route('/create_activity', methods = ['GET', 'POST'])
def create_activity():
    if request.method == 'POST':
        context = request.get_json()
        print(context)
        uid = context['user_id']
        name = context['activity_name']
        start_time = datetime(context['activity_time']['year'], 
                              context['activity_time']['month'], 
                              context['activity_time']['day'],
                              context['activity_time']['hour'], 
                              context['activity_time']['minute'], 
                              0)
        end_time = datetime(context['activity_end_time']['year'], 
                            context['activity_end_time']['month'], 
                            context['activity_end_time']['day'],
                            context['activity_end_time']['hour'], 
                            context['activity_end_time']['minute'], 
                            0)
        ddl = datetime(context['activity_registration_deadline']['year'], 
                       context['activity_registration_deadline']['month'], 
                       context['activity_registration_deadline']['day'],
                       context['activity_registration_deadline']['hour'],
                       context['activity_registration_deadline']['minute'],
                       0)
        minimum_members = context['min_people_in_team']
        maximum_members = context['max_people_in_team']
        intro = context['activity_intro']
        attention = context['activity_attention']
        new_activity = Activity(name = name,
                                start_time = start_time,
                                end_time = end_time,
                                registration_deadline = ddl,
                                auditing = 1,
                                intro = intro,
                                attention = attention,
                                minimum_members = minimum_members,
                                maximum_members = maximum_members)
        db.session.add(new_activity)
        db.session.commit()
        user = User.query.filter_by(id = uid).first()
        user.announced.append(new_activity)

        keypoints = context['keypoint_list']
        
        for keypoint in keypoints:
            latitude = keypoint['latitude']
            longitude = keypoint['longitude']
            type = keypoint['type']
            new_keypoint = Keypoint(longitude = longitude,
                                    latitude = latitude,
                                    type = type,
                                    activity_id = new_activity.id)
            db.session.add(new_keypoint)
            db.session.commit()
            new_activity.keypoint.append(new_keypoint)
        return json.dumps({'status': 0})
    return json.dumps({'status': 1})