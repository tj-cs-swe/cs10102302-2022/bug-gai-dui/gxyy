from flask import request
from . import profile
from ..models import User, register, Activity
from datetime import datetime

@profile.route('/unaudit_activity', methods = ['GET', 'POST'])
def unaudit_activity():
    ret = {}
    if request.method == 'GET':
        activities = Activity.query.filter_by(auditing = 1).order_by(Activity.start_time).all()
        menu = []
        for activity in activities:
            if activity.start_time < datetime.now():
                continue
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.start_time.year
            activity_date['m'] = activity.start_time.month
            activity_date['d'] = activity.start_time.day
            info['date'] = activity_date
            info['state'] = 4
            menu.append(info)
        ret['menu'] = menu
    return ret

