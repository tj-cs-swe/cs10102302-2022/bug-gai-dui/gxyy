from flask import request
from . import profile
from .. import db
from ..models import User, Verification
import datetime

@profile.route('/user_context_modify', methods = ['GET', 'POST'])
def user_context_modify():
    if request.method == 'POST':
        context = request.get_json()
        user = User.query.filter_by(id = int(context['uid'])).first()
        if context['username'] != user.name:
            users = User.query.filter_by(name = context['username']).all()
            if len(users) > 0:
                return {'status': 2}

        code = context['verification_code']        
        user.accurate_name = context['name']
        user.student_no = context['student No']
        user.sex = context['sex']
        user.name = context['username']

        verification = Verification.query.filter_by(mail_address = context['email']).first()
        if context['need_verify']:
            users = User.query.filter_by(mail_address = context['email']).all()
            if len(users):
                return {'status': 3}
            if (datetime.datetime.now() - verification.send_time).seconds <= 300 and code == verification.verification_code:
                user.mail_address = context['email']
                db.session.commit()
                return {'status': 0}
            else:
                return {'status': 1}
        db.session.commit()
        return {'status': 0}
    return {'status': 0}