from flask import request
from . import profile
from ..models import User, Activity, favorites
from datetime import datetime

@profile.route('/collect_activity', methods = ['GET', 'POST'])
def collect_activity():
    ret = {}
    if request.method == 'POST':
        menu = []
        context = request.get_json()
        activities = Activity.query.join(favorites).join(User)\
                                   .filter(User.name == context['username'])\
                                   .order_by(Activity.start_time).all()
        for activity in activities:
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.start_time.year
            activity_date['m'] = activity.start_time.month
            activity_date['d'] = activity.start_time.day
            info['date'] = activity_date
            if datetime.now() <= activity.registration_deadline:
                info['state'] = 0
            elif datetime.now() < activity.start_time:
                info['state'] = 1
            else:
                info['state'] = 2
            menu.append(info)
        ret['menu'] = menu
    return ret
