from flask import request
from . import profile
from ..models import User

@profile.route('/user_context', methods = ['GET', 'POST'])
def user_context():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        users = User.query.filter_by(id = int(context['uid'])).all()
        if len(users) == 0:
            ret['username'] = '未知用户'
            ret['sex'] = 0
            ret['email'] = ''
            ret['name'] = ''
            ret['student No'] = ''
            ret['isGM'] = False
        else:
            user = users[0]
            ret['username'] = user.name
            ret['sex'] = user.sex
            ret['email'] = user.mail_address
            ret['name'] = user.accurate_name
            ret['student No'] = user.student_no
            ret['isGM'] = bool(user.role)
    return ret
