from flask import request
from . import profile
from ..models import User, Activity
from datetime import datetime

@profile.route('/published_activity', methods = ['GET', 'POST'])
def published_activity():
    ret = {}
    if request.method == 'POST':
        menu = []
        context = request.get_json()
        name = context['username']
        user = User.query.filter_by(name = name).first()
        activities = Activity.query.join(User)\
                                   .filter(User.name == context['username'])\
                                   .order_by(Activity.start_time).all()
        for activity in activities:
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.start_time.year
            activity_date['m'] = activity.start_time.month
            activity_date['d'] = activity.start_time.day
            info['date'] = activity_date
            if activity.auditing == 1:
                info['state'] = 4
            else :
                info['state'] = 5
            menu.append(info)
        ret['menu'] = menu
    return ret

