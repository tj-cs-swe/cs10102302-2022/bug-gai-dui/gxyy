from flask import Blueprint

login_register = Blueprint('login_register', __name__)

from . import check_name
from . import create_account
from . import login_verify
from . import send_verification_code