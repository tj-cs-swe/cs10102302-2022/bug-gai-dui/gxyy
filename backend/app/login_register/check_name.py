from flask import request
from . import login_register
from .. import db
from ..models import User

@login_register.route('/check_name', methods = ['GET', 'POST'])
def check_name():
    if request.method == 'POST':
        context = request.get_json()
        name = context['user_name']
         # 查询
        user_list = User.query.filter_by(name = name).all()
        if len(user_list) == 0:
            return {'status': 0}
        else:
            return {'status': 1}
    return {'status': 1}