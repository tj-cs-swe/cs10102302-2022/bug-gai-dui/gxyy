from flask import request
from . import login_register
from .. import db
from ..models import User, Verification
import datetime

@login_register.route('/create_account', methods = ['GET', 'POST'])
def create_account():
    if request.method == 'POST':
        context = request.get_json()
        name = context['user_name']
        password = context['password']
        addr = context['mailbox']
        code = context['verification_code']
        verification = Verification.query.filter_by(mail_address = addr).first()
        if (datetime.datetime.now() - verification.send_time).seconds <= 300 and code == verification.verification_code:
            new_user = User(role = 0, name = name, mail_address = addr, sex = 0, accurate_name = '', student_no = '')
            new_user.set_password(password)
            db.session.add(new_user)
            db.session.commit()
            return {'status': 0}
        return {'status': 1}
    return {'status': 1}